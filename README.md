# momo-infrastructure


## Настройка облачной инфраструктуры
<details><summary>Подготовка, настройка облака, terraform</summary>

1. Регистрируемся на yandex cloud
2. Устанавливаем yc https://cloud.yandex.ru/ru/docs/cli/quickstart#install
3. Создаем сервисный аккаунт 

    `yc iam service-account create --name <имя_сервисного_аккаунта>`

    <details><summary>Добавляем роли для сервисного аккаунта</summary>

    ### Роль alb.editor нужна для создания балансировщиков
    yc resource-manager folder add-access-binding default \
    --service-account-name=**<имя_сервисного_аккаунта>** \
    --role alb.editor

    ### Роль vpc.publicAdmin нужна для управления внешними адресами
    yc resource-manager folder add-access-binding default \
    --service-account-name=**<имя_сервисного_аккаунта>** \
    --role vpc.publicAdmin

    ### Роль certificate-manager.certificates.downloader
     нужна для скачивания сертификатов из Yandex Certificate Manager
    yc resource-manager folder add-access-binding default \
    --service-account-name=**<имя_сервисного_аккаунта>** \
    --role certificate-manager.certificates.downloader

    ### Роль compute.viewer нужна для добавления нод в балансировщик
    yc resource-manager folder add-access-binding default \
    --service-account-name=**<имя_сервисного_аккаунта>** \
    --role compute.viewer 

    </details>

3. Создаем токен для аутентификации

    `yc iam key create --service-account-name ingress-controller --output sa-key.json 
`
4. Настраиваем профиль cli через  
    
    `yc init` 

    Получаем конфигурацию

    `yc config list`
5. Получаем список сетей

    `yc vpc network list`
6. Получаем список подсетей

    `yc vpc subnets list`
7. Запускаем terraform

   <details><summary>Конфигурация подключения к облаку Managed Service for Kubernetes </summary>

    terraform/terraform.tfvars
    ```
    token		= "ADD_TOKEN"
    cloud_id	= "b1g77raj2icqmj6qt2h0"
    folder_id	= "b1gdc96b9en04h1575fo"
    ```
        
    token - Полученный токен https://cloud.yandex.ru/ru/docs/iam/quickstart-overview
    
    Проверить cloud_id / folder_id -
    `yc cloud config list`
    </details>

    <details><summary>Конфигурация Managed Service for Kubernetes </summary>

    Добавляем свои параметры 
    
    terraform/provider.tf
    ```
    provider "yandex" {
    cloud_id  = "b1g77raj2icqmj6qt2h0"
    folder_id = "b1gdc96b9en04h1575fo"
    zone      = "ru-central1-a"
    token     = "ADD_TOKEN"
    }
    ```
    Конфигурируем параметры кластера в

    terraform/kuber_cluster/main.tf
    </details>

8. После создания кластера kubernetes и pod  получаем строку для kubectl

    `yc managed-kubernetes cluster get-credentials --id ID --external`

    Проверяем подключение

    `kubectl get pods -A`
9. Регистрация доменного имени и настройка CloudDNS
    <details><summary>Yandex Cloud </summary>

    1. Прописать в настройках домена DNS 

        `ns1.yandexcloud.net`
        `ns2.yandexcloud.net`   
    2. Создадим публичную в зону с помощью сервиса Yandex Cloud DNS:
        ```
        yc dns zone create \
        --name ZONE_NAME --zone <ваш домен с точкой на конце> \
        --public-visibility
        ```
    3. Создадим зарезервированный IP-адрес для балансировщика и сохраним его значение в переменной:
        ```
        yc vpc address create --name=alb \
        --labels reserved=true \
        --external-ipv4 zone=ru-central1-a
        ```

        ```
        export INFRA_ALB_ADDRESS=$(yc vpc address get alb --format json | jq -r .external_ipv4_address.address)
        ```
    4. Создадим wildcard A-запись, указывающую на IP балансировщика:
        ```
        yc dns zone add-records --name DOMAIN_NAME \
        --record "*.<домен>. 600 A $INFRA_ALB_ADDRESS” 
        ```
    5. Создадим A-запись, указывающую на IP балансировщика:
        ```
        yc dns zone add-records --name DOMAIN_NAME \
        --record ".<домен>. 600 A $INFRA_ALB_ADDRESS”
        ```
        <details><summary>Полезные команды</summary>

        ```
        export SUBNET_ID=$(yc vpc subnet get default-ru-central1-a | head -1 | awk -F ': ' '{print $2}')
        echo $SUBNET_ID 
        ```

        ```
        yc application-load-balancer load-balancer list 
        ```
        </details>
    6. Выпустить сертификаты Let’s Encrypt для wildcard A-записи и A-записи домена

        https://cloud.yandex.ru/ru/docs/certificate-manager/operations/managed/cert-create

10. Добавить содержимое полученного sa-key в

    `helm_charts\charts\yc-alb-ingress-controller-chart\values.yaml`

    `saKeySecretKey:`
11. Устанавливаем argocd, подключаем его к проекту gitlab helm_charts 

    https://cloud.yandex.ru/ru/docs/managed-kubernetes/tutorials/marketplace/argo-cd#manual_2
    
12. Проверям запущенные приложения из helm_charts
ArgoCD
<img width="900" alt="image" src="https://storage.yandexcloud.net/readme/argocd.jpg">
</details>

## Деплой приложения
<details><summary>Деплой приложения</summary>
ArgoCD
<img width="900" alt="image" src="https://storage.yandexcloud.net/readme/argocd.jpg">

Grafana
<img width="900" alt="image" src="https://storage.yandexcloud.net/readme/grafana_nginx_status.jpg">


Компоненты деплоя

Основные компоненты 

https://momo-store.fun
1. backend - приложение на go
2. frontend - статичный сайт на nginx
    
Дополнительные компоненты
1. Argocd - сервис для деплоя приложения
2. Prometheus  - сервис сбора статистики с основного приложения 

    https://prometheus.momo-store.fun 

5. Prometheus-nginx-exporter - сервис для сбора статистики с frontend nginx

3. Alertmanager - сервис для отправки оповещений о работоспособности основных компонентов 

    https://alertmanager.momo-store.fun

4. Grafana - сервис для построения графиков/отчетов 

    https://grafana.momo-store.fun

5. YC ALB Ingress controller - сервис ingress

    https://cloud.yandex.ru/ru/docs/managed-kubernetes/tutorials/alb-ingress-controller

Для доступности всех приложений развернут один Ingress контроллер 

`\momo-infrastructure\helm_charts\charts\front\templates\ingress.yaml`


</details>
