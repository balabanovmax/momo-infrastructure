resource "yandex_kubernetes_cluster" "zonal_cluster_resource_name" {
  name        = "diplom"
  description = "diplom"

  network_id = "ADD_NETWORK_ID"

  master {
    version = "1.28"
    zonal {
      zone      = "ru-central1-a"
    }

    public_ip = true


    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "15:00"
        duration   = "3h"
      }
    }

    master_logging {
      enabled = true
      kube_apiserver_enabled = true
      cluster_autoscaler_enabled = true
      events_enabled = true
      audit_enabled = true
    }
  }

  service_account_id      = "ADD_SERVICE_ACCOUNT_ID"
  node_service_account_id = "ADD_NODE_SERVICE_ACCOUNT_ID"

  labels = {
    my_key       = "diplom_key"
  }

  release_channel = "RAPID"
  network_policy_provider = "CALICO"

}
resource "yandex_kubernetes_node_group" "my_node_group" {
  cluster_id  = "${yandex_kubernetes_cluster.zonal_cluster_resource_name.id}"
  name        = "diplom"
  description = "description"
  version     = "1.28"

  labels = {
    "key" = "value"
  }

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = true
      subnet_ids         = ["ADD_SUBNET_ID"]
    }

    resources {
      memory = 4
      cores  = 2
      core_fraction = 50
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "15:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "10:00"
      duration   = "4h30m"
    }
  }
}
