terraform {
  required_version = "> 1.5.7" 
}
module "kuber_cluster" {
    	source					= "./kuber_cluster"
}