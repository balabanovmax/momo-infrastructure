1. Регистрируемся на yandex cloud
2. Устанавливаем yc https://cloud.yandex.ru/ru/docs/cli/quickstart#install
3. Создаем сервисный аккаунт 

    `yc iam service-account create --name <имя_сервисного_аккаунта>`
3. Создаем токен для аутентификации
4. Настраиваем профиль cli через  
    
    `yc init` 

    Получаем конфигурацию

    `yc config list`
5. Получаем список сетей

    `yc vpc network list`
6. Получаем список подсетей

    `yc vpc subnets list`
7. Запускаем terraform apply
8. После создания кластера kubernetes и pod - получаем строку для kubectl в интерфейсе yandex cloud - Managed Service for Kubernetes - Кластеры - выбрать кластер - Подключиться

    `yc managed-kubernetes cluster get-credentials --id ID --external`

    Проверяем подключение

    `kubectl get pods -A`

9. Устанавливаем terraform
10. Запускаем создание инфраструктуры - managed service for kubernetes и одного pod для него
11. Устанавливаем argocd, подключаем его к проекту gitlab helm_charts 

    https://cloud.yandex.ru/ru/docs/managed-kubernetes/tutorials/marketplace/argo-cd#manual_2
    
12. Проверям запущенные приложения из helm_charts
